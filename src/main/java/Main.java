import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        OutputMessages.showUserRulesMessage();
        boolean logicRunning = true;
        while (logicRunning) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            ChessDrawer myChessDrawer;
            try {
                myChessDrawer = new ChessDrawer(reader.readLine(), reader.readLine());
                myChessDrawer.checkHightForNullOrMius();
                myChessDrawer.checkWidthForNullOrMinus();
                if ((myChessDrawer.isHightNullOrMinus()) || (myChessDrawer.isWidthNullOrMinus())) {
                    OutputMessages.showUserRulesMessage();
                }
                if ((!myChessDrawer.isHightNullOrMinus()) && (!myChessDrawer.isWidthNullOrMinus())) {
                    System.out.printf(myChessDrawer.toString());
                    logicRunning = false;
                }
            } catch (IOException e){
                OutputMessages.showUserRulesMessage();
            } catch (NumberFormatException e1){
                OutputMessages.showUserRulesMessage();
            }

        }



    }
}

