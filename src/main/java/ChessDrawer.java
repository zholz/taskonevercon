public class ChessDrawer {

    private int width;
    private int hight;
    private boolean isWideNullOrMinus;
    private boolean isHightNullOrMinus;

    public boolean isWidthNullOrMinus() {
        return isWideNullOrMinus;
    }

    public boolean isHightNullOrMinus() {
        return isHightNullOrMinus;
    }

    public ChessDrawer(String wide, String hight) {
        this.width = Integer.valueOf(wide);
        this.hight = Integer.valueOf(hight);
    }

    public String toString(){
        String line;
        StringBuilder result =  new StringBuilder();
        boolean isEvenWidth = this.width%2==0;
        for (int i = 0; i<(this.width/2); i++){
            result=result.append("* ");
        }
        if (!isEvenWidth){
            result.append("*");
        }
        StringBuilder sb = new StringBuilder(result);
        for (int i = 0; i<(this.hight-1); i++){
            if (i%2==0){
                sb.append("%n").append(" ").append(result);
            } else {
                sb.append("%n").append(result);
            }

        }

        line = sb.toString();

        return line;
    }
    void checkWidthForNullOrMinus(){
        if (this.width>0){
            isWideNullOrMinus = false;
        } else{
            isWideNullOrMinus = true;
        }
    }
    void checkHightForNullOrMius(){
        if(this.hight>0){
            isHightNullOrMinus = false;
        } else {
            isHightNullOrMinus = true;
        }
    }
}
